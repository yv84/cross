from django.test import TestCase
from django.test.client import Client


class RootTestCase(TestCase):
	"""docstring for RootTestCase"""
	def setUp(self):
		# Every test needs a client.
		self.client = Client()

	def test_get_index(self):
		# Issue a GET request.
		response = self.client.get('/index/')
		# Check that the response is 200 OK.
		self.assertEqual(response.status_code, 200)

	def test_admin_page(self):
		response = self.client.get('/admin/')
		self.assertEqual(response.status_code, 200)

	# def test_login_page(self):
	# 	response = self.client.get('/login/')
	# 	self.assertEqual(response.status_code, 200)

	def test_logout_page(self):
		response = self.client.get('/logout/')
		self.assertEqual(response.status_code, 302)