import json


from django.shortcuts import render
from django.http import HttpResponse


#@login_required(redirect_field_name='login')
def index(request):
    return render(
        request, 
        'cross_app/index.html',)

def get_json_data(request): 
   return HttpResponse(json.dumps(     
            {name: 'django 1'},
            {name: 'django 2'},
            {name: 'django 3'},
            ),
        mimetype='application/json')