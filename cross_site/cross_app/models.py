from __future__ import unicode_literals
from django.utils.encoding import python_2_unicode_compatible

from django.db import models
from django.contrib.auth.models import User
from django.db.models import F  # get value
from django.db.models import Q  # OR
from django.db import transaction 
from django.utils import timezone



class ElementManager(models.Manager):

    # Element.element_manager.all()
    def get_queryset(self):
        return super(ElementManager, self).get_queryset()

    def get_parent_by_id(self, pk):
        return super(ElementManager, self).get_queryset().get(pk=pk).chield_from

    def get_children_by_id(self, pk):
        return super(ElementManager, self).get_queryset().filter(chield_from__pk=pk)

    def get_parent_by_name(self, name):
        return super(ElementManager, self).get_queryset().get(name__iexact=name).chield_from

    def get_children_by_name(self, name):
        return super(ElementManager, self).get_queryset().filter(chield_from__name__iexact=name)

    # Element.element_manager.get_root()
    def get_root(self):
        """root: element__parent__dady_id = element__parent_id = element_id = 1"""
        return super(ElementManager, self).get_queryset().get(id__exact=1)

    def get_ancestors(self, pk):
        ancestors = []
        while pk != 1:
            ancestor = self.get_parent_by_id(pk)
            pk = ancestor.pk
            ancestors.append(ancestor.pk)
        ancestors.reverse() # dict
        return super(ElementManager, self).get_queryset().filter(pk__in=ancestors).order_by()


    def get_chains(self, pk):
        return Chain.objects.filter(chains_set=super(ElementManager, self).get_queryset().get(id__exact=pk))


class ChainManager(models.Manager):
    def get_queryset(self):
        return super(ChainManager, self).get_queryset()

    def get_chain_by_id(self, pk):
        return super(ChainManager, self).get_queryset().get(pk=pk)

    def get_chain_by_name(self, name):
        return super(ChainManager, self).get_queryset().filter(name__iexact=name)


#@transaction.atomic
class AbstractBase(models.Model):
    """
    An abstract base class model that provides selfupdating ``created`` and ``modified`` fields.
    """
    # istoriya izmeneniu' ne podderjivaetsya
    created = models.DateTimeField(auto_now_add=True, verbose_name="created")
    modified = models.DateTimeField(auto_now=True, verbose_name="saved")
    user = models.ForeignKey(User, verbose_name="related user", blank=False, null=False)
    objects = models.Manager() # The default manager.

    class Meta:
        abstract = True


class AbstractElement(AbstractBase):
    """docstring for AbstractElement"""
    name =  models.CharField(max_length=20, blank=False, null=False)
    slug = models.SlugField(max_length=20, blank=False, null=False)
    description = models.CharField(max_length=64, blank=True, null=False)

    def __str__(self):
        return "%s" %(self.name,)

    class Meta:
        ordering = ["name"]
        abstract = True


@python_2_unicode_compatible
class Chain(AbstractElement):
    """connection between elements"""
    chain_manager = ChainManager()
    
    class Meta(AbstractElement.Meta):
        verbose_name_plural = "chains"


@python_2_unicode_compatible
class Element(AbstractElement):
    """Element on any cross"""

    chield_from = models.ForeignKey('self', related_name='chield_from_set', verbose_name="chield from", blank=False, null=False)
    chains = models.ManyToManyField(Chain, related_name='chains_set', verbose_name="chains", through='Link')

    def save(self, *args, **kwargs):
        if (self.pk != 1 and self.name == 'root') or (self.pk == 1 and self.name != 'root'):
            return # one root for a database
        else:
            super(Element, self).save(*args, **kwargs)

    element_manager = ElementManager()
 
    class Meta(AbstractElement.Meta):
        verbose_name_plural = "elements"


@python_2_unicode_compatible
class Link(AbstractBase):
    """chain <-> element"""

    element = models.ForeignKey(Element, verbose_name="related element", blank=False, null=False)
    chain = models.ForeignKey(Chain, verbose_name="related chain", blank=False, null=False)
    order = models.IntegerField(verbose_name="order in chain", blank=False, null=False)

    def __str__(self):
        return "%s" %(self.name,)

    class Meta:
        ordering = ["order"]
        verbose_name_plural = "links"


# forms