from django.test import TestCase
from cross_app.models import Element
from cross_app.models import Chain
from cross_app.models import Link



class ElementTestCase(TestCase):
    def setUp(self):
        root = Element.objects.create(pk=1, name='fake', user_id=1, 
            chield_from_id=1)
        root = Element.objects.create(pk=1, name='root', user_id=1, 
            chield_from_id=1)
        root.chield_from_id = root.pk
        b11 = Element.objects.create(name='b11', user_id=1, chield_from_id=1)
        b12 = Element.objects.create(name='b12', user_id=1, chield_from_id=1)
        b13 = Element.objects.create(name='b13', user_id=1, chield_from_id=1)
        b14 = Element.objects.create(name='b14', user_id=1, chield_from_id=1)
        b21 = Element.objects.create(name='b121', user_id=1, chield_from=b12)
        b22 = Element.objects.create(name='b122', user_id=1, chield_from=b12)
        b23 = Element.objects.create(name='b123', user_id=1, chield_from=b12)
        b24 = Element.objects.create(name='b124', user_id=1, chield_from=b12)

        c1 = Chain.objects.create(name='c1', user_id=1)
        c2 = Chain.objects.create(name='c2', user_id=1)
        c3 = Chain.objects.create(name='c3', user_id=1)

        l11 = Link.objects.create(element=b11, chain=c1, order=1, user_id=1)
        l12 = Link.objects.create(element=b22, chain=c1, order=2, user_id=1)
        l13 = Link.objects.create(element=b23, chain=c1, order=3, user_id=1)
        l14 = Link.objects.create(element=b24, chain=c1, order=4, user_id=1)
        l15 = Link.objects.create(element=b13, chain=c1, order=5, user_id=1)

        l21 = Link.objects.create(element=b13, chain=c2, order=1, user_id=1)
        l22 = Link.objects.create(element=b14, chain=c2, order=2, user_id=1)
        l23 = Link.objects.create(element=b24, chain=c2, order=3, user_id=1)
        l24 = Link.objects.create(element=b23, chain=c2, order=4, user_id=1)
        l25 = Link.objects.create(element=b11, chain=c2, order=5, user_id=1)

    @staticmethod
    def list_elements(obj, j):
        elements = []
        for k in j:
            elements.append(obj.objects.get(name=k))
        return elements

    def test_element_model(self):
        """test element model"""
        Element.objects.create(name='root', user_id=1, chield_from_id=1)
        self.assertEqual(Element.objects.get(name='root').pk, 1)
        for i, j in zip(['root', 'b11', 'b121'],['root', 'root', 'b12']):
            self.assertEqual(Element.objects.get(name=i).chield_from_id, 
                Element.objects.get(name=j).pk)
        self.assertEqual(Element.objects.filter(name='root').count(), 1)
        for i, j in zip(['root', 'b12', 'b124'],[5, 4, 0]): # 4 + 1 root
            self.assertEqual(Element.objects.filter(
                chield_from=Element.objects.get(name=i)).count(), j) 
        # element manager
        # get parent
        self.assertEqual(Element.element_manager.filter(
            chield_from=Element.objects.get(name='b12')).count(), 4)
        for i, j in zip(['b12', 'b124'],['root', 'b12']):
            self.assertEqual(Element.element_manager.get_parent_by_id(
                Element.element_manager.get(name=i).pk), 
                Element.objects.get(name=j))
            self.assertEqual(Element.element_manager.get_parent_by_name(
                Element.element_manager.get(name=i).name), 
                Element.objects.get(name=j))

        # get chield
        self.assertEqual(Element.element_manager.filter(
            chield_from=Element.objects.get(name='b12')).count(), 4)
        for i, j in zip(['root', 'b12', 'b122'],[5,4,0]):
            self.assertEqual(Element.element_manager.get_children_by_id(
                Element.element_manager.get(name=i).pk).count(), j)
            self.assertEqual(Element.element_manager.get_children_by_name(
                Element.element_manager.get(name=i).name).count(), j)

        # get root
        self.assertEqual(Element.element_manager.get_root(), 
            Element.objects.get(name='root'))

        for i, j in zip(['c1','c2','c3'],[5,5,0]):
            self.assertEqual(Element.objects.filter(
                chains=Chain.objects.filter(name=i)).count(), j)

        # get ancestors from element pk
        for i, j in zip(('root', 'b12', 'b121'),
                ([], ['root',], ['root', 'b12'])):
            self.assertEqual(list(Element.element_manager.get_ancestors(
                Element.element_manager.get(name=i).pk)),
                ElementTestCase.list_elements(Element, j))

        # get chains for element from pk
        for i, j in zip(
                ['root','b11','b12','b13','b14','b121','b122','b123','b124'],
                [[],['c1','c2',],[],['c1','c2',],['c2'],
                    [],['c1'],['c1','c2',],['c1','c2',]]):
            self.assertEqual(list(Element.element_manager.get_chains(
                Element.element_manager.get(name=i).pk)), 
                ElementTestCase.list_elements(Chain, j))


    def test_chain_model(self):
        """test chain model"""
        self.assertEqual(Chain.objects.filter(name='c1').count(), 1)
        self.assertEqual(Chain.objects.filter(name='c2').count(), 1)
        self.assertEqual(Chain.objects.filter(name='c3').count(), 1)
        for i, j in zip(
                ['root','b11','b12','b13','b14','b121','b122','b123','b124'],
                [0,2,0,2,1,0,1,2,2]):
            self.assertEqual(Chain.objects.filter(
                chains_set=Element.objects.filter(name=i)).count(), j)
        # chain manager
        self.assertEqual(Chain.chain_manager.filter(name='c1').count(), 1)
        for i in ['c1', 'c2', 'c3']:
            self.assertEqual(Chain.chain_manager.get_chain_by_id(
                Chain.chain_manager.get(name=i).pk), 
                Chain.objects.get(name=i))
        for i, j in zip(['c1', 'c2', 'c3'],[1,1,1]):
            self.assertEqual(Chain.chain_manager.get_chain_by_name(
                Chain.chain_manager.get(name=i).name).count(), j)


    def test_link_model(self):
        """test link model"""
        for i, j in zip(['c1','c2','c3'],[5,5,0]):
            self.assertEqual(Link.objects.filter(chain__name=i).count(), j)
        for i, j in zip(range(1, 6), ['b11', 'b122', 'b123', 'b124', 'b13']):
            self.assertEqual(Link.objects.filter(
                chain__name='c1').get(order=i).element, 
                Element.objects.get(name=j))
        for i, j in zip(range(1, 6), ['b13', 'b14', 'b124', 'b123', 'b11']):
            self.assertEqual(
                Link.objects.filter(chain__name='c2').get(order=i).element, 
                Element.objects.get(name=j))