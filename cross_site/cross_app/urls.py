from django.conf.urls import patterns, include, url


urlpatterns = patterns('',
    url(r'^index/$', 'cross_app.views.index'),
    url(r'^json_data/$', 'cross_app.views.get_json_data'),
)
