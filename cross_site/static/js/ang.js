(function() {
  var config, cross_app_ng, x;

  cross_app_ng = angular.module('cross_app_ng', []);

  cross_app_ng.config(function($interpolateProvider) {
    $interpolateProvider.startSymbol('{[{');
    $interpolateProvider.endSymbol('}]}');
  });

  config = {
    params: {
      id: '5',
      'X-CSRFToken': csrftoken
    }
  };

  x = $http.post('json_data', postData, config).success(function(data, status, headers, config) {
    console.log('successful');
  }).error(function(data, status, headers, config) {
    console.log('error');
  });

  cross_app_ng.controller('cross_app_ng_controller', function($scope) {
    $scope.input_cross_app = {
      text: 'Hello'
    };
    $scope.chields_elements = [
      {
        name: 'Paint pots'
      }, {
        name: 'Polka dots'
      }, {
        name: 'Pebbles'
      }
    ];
    $scope.parents_elements = [
      {
        name: 'Paint pots'
      }, {
        name: 'Polka dots'
      }, {
        name: 'Pebbles'
      }
    ];
    $scope.chains_elements = [
      {
        name: 'Paint pots'
      }, {
        name: 'Polka dots'
      }, {
        name: 'Pebbles'
      }
    ];
    /*    # button click
    $scope.chield = function(i){
      console.log('here');
    
    }
    */

    $scope.parent = function(i) {
      return console.log('here');
    };
    $scope.chain = function(i) {
      return console.log('here');
    };
    $scope.insert_element = function() {};
  });

}).call(this);
